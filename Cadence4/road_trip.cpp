
/*******************************************
 * road_trip.cpp
 * Author: Owen Gallahue
 * Date: 2019-04-14
 * Google Question, Cadence 4
 * Road Trip Game
 ******************************************/

#include <iostream>
#include <algorithm>
#include <set>
#include <string>
#include <fstream>
#include <iterator>
#include <string.h>

/************************
 * Dictionary struct
 * Slurps words from /usr/share/dict/words
 * Source: https://stackoverflow.com/questions/2211097/dictionary-library-in-c
 ************************/

struct Dictionary {
	Dictionary() {
		std::ifstream input("words.txt");
		for(std::string line; getline(input, line);){
				std::transform(line.begin(), line.end(), line.begin(), ::toupper);
				words_set.insert(line);
		}
	}
	std::set<std::string> words_set;
};


/************************
 * extractLetter
 * Preconditions: std::string
 * Postconditions: std::string
 *
 * Returns the letters from a string of any type of character
 ************************/
std::string extractLetter(std::string plate){
	std::string result = "";
	for(char c : plate){
		if(c >= 65 && c <=90)
			result += (c);
	}
	return result;
}

/************************
 * main
 * Preconditions: None
 * Postconditions: int
 *
 * Min driver function to find shortest word that uses all the letters from a given license plate.
 ************************/
int main(){
	struct Dictionary newDict;

	// license plate
	std::string plate = "AEI-1O2U3";

	// extracted letters from license plates
	std::string letters = extractLetter(plate);
	// longest word in the dictiornary, therefore found word
	// gonna have to be smnaller that this 
	// Found using:
	// cat words.txt | awk ' { if ( length > x ) { x = length; y = $0 } }END{ print y }'
	std::string MATCH = "pneumonoultramicroscopicsilicovolcanoconiosis";
	for (std::string s : newDict.words_set){
		// gotCha is counting to make sure all the letters in the plate are used int the word.
		int gotCha = 0;
		for(char k : letters){
			//std::cout<< "Looking for the letter: " << k << " in word: " << s <<std::endl;
			if(s.find(k) !=std::string::npos){
				gotCha++;	
			}
		}

		// gotCha will equal the length of the letters found if all letters
		// are in that word
		if(gotCha == letters.length() && s.length() < MATCH.length()){
			MATCH = s;
		}
	}

	std::cout << "For plate '" << plate << "' found letters: " << letters << " found word: ";
	std::cout << MATCH << std::endl;
	return 0;

}


