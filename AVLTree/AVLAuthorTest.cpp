/**********************************************
* File: AVLAuthorTest.cpp
* Author: Owen Gallahue 
* Email: ogallahu@nd.edu
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here
struct Author{
	std::string FirstName;
	std::string LastName;

	Author(std::string FirstName, std::string LastName) : FirstName(FirstName), LastName(LastName) {}
	//Overloaded operator
	bool operator<(const Author& rhs) const{
	// lhs < rhs
	if(LastName < rhs.LastName)
		return true;
	else if(LastName == rhs.LastName){
		if(FirstName < rhs.FirstName)
			return true;
		}
	return false;
	}
	//Overloaded operator
	bool operator==(const Author& rhs) const{
		if(LastName != rhs.LastName)
			return false;
		else{
			if(FirstName != rhs.FirstName)
				return false;
		}
		return true;
	}

	// Overloaded operator
	friend std::ostream& operator<<(std::ostream& outStream, const Author& printAuth);
				
};


std::ostream& operator<<(std::ostream& outStream, const Author& printAuth){

	outStream << printAuth.LastName << ", " << printAuth.FirstName;

	return outStream;
}
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
    // Create initiual AVLTree.
    AVLTree<Author> authorsAVL;
    Author Aardvark("Anthony", "Aardvark");
    Author Aardvark2("Greg", "Aardvark");
    Author McDowell("Gayle", "Mcdowell");
    Author Main("Michael", "Main");
    Author BadPerson("Badmean", "Person");

    authorsAVL.insert(Aardvark);
    authorsAVL.insert(Aardvark2);
    authorsAVL.insert(McDowell);
    authorsAVL.insert(Main);
    authorsAVL.insert(BadPerson);

    authorsAVL.printTree();

    std::cout << "Removing " << BadPerson << std::endl;
    authorsAVL.remove(BadPerson);
    authorsAVL.printTree();
    return 0;
}
