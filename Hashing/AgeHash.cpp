
/**********************************************
 * * File: AgeHash.cpp
 * * Author: Matthew Morrison
 * * Email: matt.morrison@nd.edu
 * * 
 * * Shows the comparison of the input or ordered and
 * * unordered sets 
 * **********************************************/


#include <map>
#include <unordered_map>
#include <iterator>
#include <iostream>
#include <string>


/********************************************
* function Name  : main
* Pre-conditions : int argc, char** argv
* * Post-conditions: int
* *
* * Main driver function. Solution  
* ********************************************/
int main(int argc, char** argv){

    std::map<std::string, int> ageHashOrdered = { {"Owen", 25}, {"Reed", 38},{"Tanner", 18},{"Max", 15},{"Nick", 21} };
    std::map<std::string , int>::iterator iterOr;
    std::cout << "the Ordered Hashers are: " << std::endl;

   for(iterOr = ageHashOrdered.begin();iterOr != ageHashOrdered.end(); iterOr++){
	std::cout << iterOr->first << " " << iterOr->second << std::endl;
    }

    std::unordered_map<std::string, int> ageHashUnordered = { {"Owen", 25}, {"Reed", 38},{"Tanner", 18},{"Max", 15},{"Nick", 21} };
    std::unordered_map<std::string , int>::iterator iterUn;
    std::cout << "------------------" << std::endl << "The Unordered Hashes are:"<<std::endl;

   for(iterUn = ageHashUnordered.begin();iterUn != ageHashUnordered.end(); iterUn++){
        std::cout << iterUn->first << " " << iterUn->second << std::endl;
    }
    std::cout << std::endl;
    std::cout << "The Ordered Example: " << ageHashOrdered["Owen"] << std::endl;
    std::cout << "The Unordered Example: " << ageHashUnordered["Tanner"] << std::endl;
    return 0;	
}




