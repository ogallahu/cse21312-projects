#include <iostream>
#include <string.h>
using namespace std;

/**
 * Checks if X has a winning board.
 * @param   board           The 3X3 ticTacToe Board
 * @param   size            How many row/columns there are, e.g. 3.
 * @return  wether or not X has a winning board.
 */
bool XWinner(char board[][3], int size){
    int diagonalSums[2] = { 0 };
    int *columnSums= new int[size];
    memset(columnSums, 0, sizeof columnSums);
    int rowSum = 0;
    for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                     if (board[i][j] == 'X'){
                            rowSum++;
                            columnSums[j]++;
                            if (i == size-1 &&  columnSums[j] == size) return true;
                            else if (i == j){
                                     diagonalSums[0]++;
                                     if (i == size-1 &&  diagonalSums[0] == size) return true;
			    }
                            else if (i == size-1-j){
                                     diagonalSums[1]++;
                                     if (j == 0 &&  diagonalSums[1] == size) return true;
			    }
			
            }
     		 
            if (rowSum == size) return true;
            else 
		rowSum = 0;
    } }
     return false;

}
/**
 * Checks if O has a winning board.
 * @param   board           The 3X3 ticTacToe Board
 * @param   size            How many row/columns there are, e.g. 3.
 * @return  wether or not O has a winning board.
 */

bool OWinner(char board[][3], int size){
    int diagonalSums[2] = { 0 };
    int *columnSums= new int[size];
    memset(columnSums, 0, sizeof columnSums);
    int rowSum = 0;
    for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                     if (board[i][j] == 'O'){
                            rowSum++;
                            columnSums[j]++;
                            if (i == size-1 &&  columnSums[j] == size) return true;
                            else if (i == j){
                                     diagonalSums[0]++;
                                     if (i == size-1 &&  diagonalSums[0] == size) return true;
                            }
                            else if (i == size-1-j){
                                     diagonalSums[1]++;
                                     if (j == 0 &&  diagonalSums[1] == size) return true;
                            }

            }

            if (rowSum == size) return true;
            else
                rowSum = 0;
    } }
     return false;

}

int main(){
	int n = 3;
	char board[3][3] = 
	{
		{'O','X','X'}, 
		{'X','X','O'},
		{'O','X','O'}
	};
	bool X,O;
	X = XWinner(board, n);
	O = OWinner(board, n);
	if(X) cout<< "X wins!"<<endl;
	else if(O) cout << "O wins!" <<endl;
	else cout<< "Cats Game!" <<endl; 
} 
