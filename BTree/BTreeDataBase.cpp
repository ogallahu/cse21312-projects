#include "BTree.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
struct Data{
	int key;
	int data;
	Data(int key, int data) : key(key), data(data) {}
	Data() : key(1),data(1) {}	
	//Overload operator
	bool operator==(const Data& rhs) const{
        	if(key != rhs.key)
                        return false;
                else{
                        if(key != rhs.key)
                                return false;
                }
                return true;
        }
	int getKey(){ return key;}
	// Overload operator
        bool operator<(const Data& rhs) const{
        	if (key < rhs.key)
                	return true;
        	return false;
        }
	//Overload Operator
        bool operator>(const Data& rhs) const{
                if (key > rhs.key)
                        return true;
                return false;
        }
	friend std::ostream& operator<<(std::ostream& outStream, const Data& point);
	};

std::ostream& operator<<(std::ostream& outStream, const Data& point){

	outStream << point.key << ", " << point.data;

	return outStream;
}

int main(int argc, char **argv){
	int c, key, val;
	srand (time(NULL)); // Used for random num generator
	BTree<Data> BData(2);
	while(c<1000){
		key = rand()%100 +1;
		val = rand()%100 +1;
		Data point(key,val);
		BData.insert(point);
		c++;
	}
	for(int l = 0; l<10;l++){
		key = rand()%100 +1;
		std::cout << "Seraching for key of value " << key << ". Found: "<<std::endl;	
		Data search(key, 1);
		BData.printFoundNodes(search);
	}
	return 0;
}
